using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHealthPointComponent : MonoBehaviour
{
    [SerializeField] 
    public const float MAX_HP = 100;

    [SerializeField] private float m_Healthpoint;

    public float HealthPoint
    {
        get
        {
            return m_Healthpoint;
        }
        set
        {
            if (value > 0)
            {
                if (value <= MAX_HP)
                {
                    m_Healthpoint = value;
                }
                else
                {
                    m_Healthpoint = MAX_HP;
                }
            }
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
